# Woastel Home Page

Die Home Page von woastel die domain ist woastel.com. Diese Home Page wurde mit React.js erstellt. Mit dem Tool `create-react-app`.

- [x] Implementieren des Routers `react-router`.  
- [ ] Implementierung `redux`, `react-redux` und `redux-thunk`.  
- [ ] Notes: Vorgehensweise (setup, implementierung) von React, ReactRouter, ReactRedux.  

## Hier geht zu den TUTs

- [Part1: React und Router](TUT_React_Part1_Router.md)
- [Part2: React und Redux](TUT_React_Part2_Redux.md)
