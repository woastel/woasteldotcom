# [TUT] Part 2: Redux als State Manager

Zu Part1 des Tutorials [Link zu Part1](TUT_React_Part1_Router.md)

In Part 2 des **React Base Projekt** Tutorial. Fügen wir unserer anwendung noch einen State Manager hinzu.

Im grunde brauchen wir um mit Redux und `react-redux` arbeiten wollen follgende Komponenten:

- Einen Provider der das Konzept zur verfügung stellt.
- Deiser Provider braucht einen Store indem er die `states` speichern kann.
- Einen Reducer der sich um die `states` im store kümmert
- Und Actions die eine Änderung einer state im store verursacht und die neues states an ihre `components` übermittelt.
- Zu guter letzt die `React.Component` Objects die Actions auslösen. Oder States auswerten bzw. anzeigen können.

Wir fangen aber mit der installation von Redux an.

## Install Redux

Das setup von redux ist ein bisschen aufwendiger aber auch nicht unmöglich. Wir verwedenen die von redux bereitgestellte lib `react-redux`. Diese lib ist eine Art Schnittstelle zu der JavaScript-Library `redux`. Deswegen müssen wir auch beide libs installieren. Auserdem müssen wir noch eine Speicher `store` einrichten.  

Redux Links:

- [Redux Getting Started](https://redux.js.org/introduction/getting-started)
- [Redux Usage with React](https://redux.js.org/basics/usage-with-react)
- [React-Redux](https://react-redux.js.org/)

Install Redux und React-Redux:  

```bash
npm install redux
npm install react-redux
npm install redux-thunk
```

Hier noch die Links zu den jeweiligen Git Repo's:

- Redux
- React Redux
- [React Thunk](https://github.com/reduxjs/redux-thunk)

> Exkursion: Was sind Thunks? Hier ist ein Tutorial über Thunks [https://daveceddia.com/what-is-a-thunk/](https://daveceddia.com/what-is-a-thunk/)  

## Neue Projekt Struktur

Bevor wir mit Redux weitermachen.
Strukturieren wir unseren Code ein bisschen um.
Dazu erstellen wir einen neuen Ordner mit dem namen `pages`.
In diesem Ordner werden wir unsere Pages lagern die wir mit dem React Router aufrufen.
React Router ist aus Part1 [TUT **React Base Projekt** Part 1: Router](TUT_React_Part1_Router.md).

Wir erstellen drei neue Dateien für jede Router Page Komponente eine, diese nennen wir `HomePage.js`, `AboutPage.js` und `BacksidePage.js`.  

Hier der Inhalt der Dateien.

Inhalt **HomePage.js**:

```javascript
// src/pages/HomePage.js

```

Inhalt **AboutPage.js**:

```javascript
// src/pages/AboutPage.js

```

Inhalt **BacksidePage.js**:

```javascript
// src/pages/BacksidePage.js

```

Jetzt müssen wir die App.js Datei noch modifizieren.

Inhalt **App.js**:

```javascript
// src/App.js

```

Ein Screenshot der Folderstrucktur.

Jetzt haben wir einen Ordner für unsere Seiten `pages`. Diese bestehen aus Componenten. Es bietet sich an die `components` der Seiten auch in diesen Ordnern in eigenen Dateien zu Speichern. Wir erstellen aber direct in unserem `src` verzeichniss einen Ordner mit dem namen `components`.
In diesen Ordner kommen all unsere Komponenten.
Den Anfang machen wir mit der **Head Line** aus unserem `App.js` File.

Dafür erstellen wir ein neues File namens `HeadLine.js` in unserem frisch erstelltem Ordner `components`.

Der Inhalt der Datei schaut so aus:

```javascript
// src/components/HeadLine.js

```

Jetzt implementieren wir die `<HeadLine />` Komponente noch in unserem `App.js` File.

```javascript
// src/App.js

```

Jetzt haben wir die altlasten neu struckturier und können uns weiter an redux wenden.  
Nun kommen wir zum anfang der implementierung der Redux Library. Dazu erstellen wir in unserem `src` (Source Folder) einen Ordner Names `redux`. In diesem Ordner erstellen wir zwei weitere Ordner mit den Namen `actions` und `reducers`.

## Redux implementieren

Redux speichert den Inhalt der `states` bzw. `props` einer `React.Component` Klasse in einen sog. `store` der widerum anderen `React.Component` Klassen zur verfügung steht.  
Wir wollen uns zwei `React.Component`'s erstellen.
Eine solle die Hintergrund Farbe ändern.
Die andere soll das `event` zum ausführen geben.  

Wir erstellen in dem Ordner `componentes` follgende Dateien `StyleBox.js` und `StyleChangeBtn.js`.

### StyleBox Component

Inhalt der StyleBox.js Datei:

```javascript
// src/componentes/StyleBox.js
```

### StyleChangeBtn Component

Inhalt der StyleChangeBtn.js Datei:

```javascript
// src/componentes/StyleChangeBtn.js
```

## Reducer erstellen

Reducer bedeutet auf Deutsch "Reduzierstück".  
Wir erstellen für unseren Reducer ein extra File in dem Ordner `reducers` Namens `styleReducer.js`.  

Mit follgendem Code:  

```javascript
import { FETCH_POSTS, NEW_POST } from '../actions/types';

const initialState = {
  items: [],
  item: {}
}

export default function(state = initialState, action ) {
  switch(action.type) {
    case FETCH_POSTS:
      console.log('fetch posts reduce2r');
      return {
        ...state,
        items: action.payload
      }
    case NEW_POST:
      return {
        ...state,
        item: action.payload
      }
    default:
      return state;
  }
}
```

Zum besseren handling erstellen wir noch einen `index.js` Datei, mit follgendem Inhalt.  

```javascript
import { combineReducers } from 'redux';
import styleReducer from './styleReducer';

export default combineReducers({
  stlye: styleReducer
});
```

## Action's und `redux-thunk`

In dem Ordner `actions` erstellen wir eine Datei Namens `types.js`. In dieser Datei definieren wir die Action Typen.  

Inhalt der Datein `types.js`:  

```javascript  
export const SET_STYLE = "SET_STYLE";
```

Jetzt müssen wir noch einen Action definieren. Dafür erstelen wir ein weiteres File namens `styleActions.js`.  

```javascript  

```

## Store erstellen

Damit Redux die `states` speichern kann brauchen wir noch einen Store. Dazu erstellen wie in dem Ordner `lalalla` ein File namens `store.js`. Und tragen follgenden Ihnalt in das File.  

```javascript

```

## Provider Component implementieren

Jetzt haben wir den store erstellt. Den store benutzen wir indem wir ein Provider Object `<Provider>` erstellen. Und ihm als Property `prop` den store übergeben.  

Unser `App.js` File sieht jetzt wie follgt aus:  

```javascript

```
